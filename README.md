# purpose

mitigates issue of wayland apps crashing when they don't read the socket buffer quickly enough (buffer fills up, then the wayland server terminates the connection)

a huge problem with 8khz mice!

works by hooking accept() and accept4(), on call increases the send socket buffer to the maximum for accepted unix stream sockets that match the wayland socket path

# use

## increase your maximum socket write buffer size

as root: create ```/etc/sysctl.d/waylandfix.conf``` containing the following:

```
net.core.wmem_max=16777216
```

also as root: run ```sysctl --system``` to apply changes

## compile

run ```make``` to compile

## change sway startup command

alter your sway command from say:

```
/path/to/bin/sway
```

to:

```
/lib64/ld-linux-x86-64.so.2 --preload /home/blah/accepthack/accepthack.so /path/to/bin/sway
```

and restart sway and hopefully the problem will now be mitigated!
